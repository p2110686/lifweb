const array1 = ["a", "b", "c"];
const array2 = [1,2,3];

for (const element of array1) {
  console.log(element);
}

// Expected output: "a"
// Expected output: "b"
// Expected output: "c"
function incrementer(x) {
    return x + 1;
}
for (variable of array2){
    console.log(incrementer(variable));
    
}

// for in 

for (variable1 in array2){
    //console.log(incrementer(variable1));
    console.log(incrementer(array2[variable1]));
}

// Oslo : lat: 59.8939243, lon: 10.6203135
/* Exercice 1 : Afficher la météo de coordonnées lat/long*/


// version 0.1

/*
async function getMeteoLatLon(lat, lon) {
    const url = `https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=${lat}&lon=${lon}`;
    const response = await fetch(url);
    if (response.ok) {
        let data = await response.json();
        console.log(data.properties.timeseries[0]);
        const temperature = data.properties.timeseries[0].data.instant.details.air_temperature;
        const meteoSymbol = data.properties.timeseries[0].data.next_1_hours.summary.symbol_code;
        return {temperature, meteoSymbol};
    } else {
        console.error('Erreur HTTP: ' + response.status);
    }

}
async function exo1MeteoOslo(lat,long){
    const res = await getMeteoLatLon(lat,long);
    document.querySelector("#meteo").innerHTML = `Il fait ${res.temperature}°C à Oslo et le temps est ${res.meteoSymbol}`;
    document.querySelector("#meteo-oslo .temperature").innerText = res.temperature;
    document.querySelector("#meteo-oslo .meteo-icon").data = 'svg/${res.meteoSymbol}.svg';
}
exo1MeteoOslo(59.8939243, 10.6203135);

*/

// version 0.2 optimiser avec .then othen .catch
async function getMeteoLatLon(lat, lon) {
   const url = `https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=${lat}&lon=${lon}`;
    return fetch(url)
         .then(response => {
              if (response.ok) {
                return response.json();
              } else {
                console.error('Erreur HTTP: ' + response.status);
              }
         })
         .catch(error => {
              console.error('Erreur fetch: ' + error);
         });
}

async function exo1MeteoOslo(lat, long) {
    const data = await getMeteoLatLon(lat, long);
    const temperature = data.properties.timeseries[0].data.instant.details.air_temperature;
    const meteoSymbol = data.properties.timeseries[0].data.next_1_hours.summary.symbol_code;
    document.querySelector("#meteo-oslo").innerHTML = `Il fait ${temperature}°C à Oslo et le temps est ${meteoSymbol}`;
    document.querySelector("#meteo-oslo .temperature").innerText = temperature;
    document.querySelector("#meteo-oslo .meteo-icon").data = `svg/${meteoSymbol}.svg`;
    
}
exo1MeteoOslo(59.8939243, 10.6203135);

/*Exercice 2 : Afficher la météo d’une ville donnée*/
async function getCoordonneesVille(ville) {
    const url = `https://data.geopf.fr/geocodage/search?&q=${ville}`;
    return fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                console.error('Erreur HTTP: ' + response.status);
            }
        })
        .catch(error => {
            console.error('Erreur fetch: ' + error);
        });
}

async function exo2MeteoLyon(ville) {
    const data = await getCoordonneesVille(ville);
    const lat = data[0].lat;
    const lon = data[0].lon;
    const meteo = await getMeteoLatLon(lat, lon);
    const temperature = meteo.properties.timeseries[0].data.instant.details.air_temperature;
    const meteoSymbol = meteo.properties.timeseries[0].data.next_1_hours.summary.symbol_code;
    document.querySelector("#meteo-lyon").innerHTML = `Il fait ${temperature}°C à Lyon et le temps est ${meteoSymbol}`;
    document.querySelector("#meteo-lyon .temperature").innerText = temperature;
    document.querySelector("#meteo-lyon .meteo-icon").data = `svg/${meteoSymbol}.svg`;
}
exo2MeteoLyon('Lyon');

     


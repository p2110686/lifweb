"use strict";
console.info("app.js loading...");

console.info("...app.js loaded");

/* Exercice 0  */


// 1. On récupère les éléments de la page 
const ul = document.querySelector("ul");
const lis = ul.querySelectorAll("li");

// 2. On affiche les éléments dans la console
console.log(ul);
console.log(lis);

// 3. On affiche le nombre d'éléments dans la console
console.log(lis.length);



/* Exercice 1  */
const decrementBtn = document.getElementById("decrementBtn");
const incrementBtn = document.getElementById("incrementBtn");
const counter = document.getElementById("counter");

decrementBtn.addEventListener("click", () => {
  counter.textContent = Number(counter.textContent) - 1;
});

incrementBtn.addEventListener("click", () => {
  counter.textContent = Number(counter.textContent) + 1;
});


const resetBtn = document.getElementById("resetBtn");
resetBtn.addEventListener("click", () => {
  counter.textContent = 0;
});


const myText = document.getElementById('myText');
myText.addEventListener('click', () => {
  myText.textContent = 'Hello World';
});

/* Exercice 2 */

const emailInput = document.getElementById("emailInput");
const validateBtn = document.getElementById("validate-btn");
const errorMessage = document.getElementById("errorMessage");

// verify the email
emailInput.addEventListener("input", () => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    Boolean(emailRegex.test(emailInput.value)) ? errorMessage.textContent = "" : errorMessage.textContent = "Email invalide";

});
// validate the email
validateBtn.addEventListener("click", () => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    Boolean(emailRegex.test(emailInput.value)) ? errorMessage.textContent = "" : errorMessage.textContent = "Email invalide";
    
});
// reset the email
resetBtn.addEventListener("click", () => {
    emailInput.value = "";
    errorMessage.textContent = "";
});

// color case email red for error  et green for ok
emailInput.addEventListener("input", () => {
    Boolean(emailRegex.test(emailInput.value))? emailInput.style.color = "green" : emailInput.style.color = "red";
});
 









